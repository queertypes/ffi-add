# Haskell FFI to C: Addition

A simple FFI example. Implements an addition function in C and exposes
it in Haskell.

The key bits are:

* cabal file:

```haskell
  c-sources:           cbits/add.c
  include-dirs:        include
  includes:            add.h
  install-includes:    add.h
```

* `cbits` directory, containing C implementation
* `include` directory, exposing C header files
* `Data.Add.Internal`, connecting C and Haskell
