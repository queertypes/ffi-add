{-# LANGUAGE ForeignFunctionInterface #-}
module Data.Add.Internal (
  c_add
) where

import Foreign.C.Types

foreign import ccall unsafe "static add.h add" c_add
  :: CInt -> CInt -> IO Int
