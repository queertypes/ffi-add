module Data.Add where

import Data.Add.Internal

add :: Int -> Int -> IO Int
add x y = (c_add (fromIntegral x) (fromIntegral y))
